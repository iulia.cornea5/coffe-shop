package coffeshop.products.food.enums;

public enum SandwichType {
    VEGAN, WITH_MEAT
}
