package coffeshop.products.food.enums;

public enum CakeType {
    CHEESE_CAKE, CARROT_CAKE, COOKIE
}
