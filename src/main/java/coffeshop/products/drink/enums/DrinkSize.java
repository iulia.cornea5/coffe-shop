package coffeshop.products.drink.enums;

public enum DrinkSize {
    SMALL, MEDIUM, LARGE
}
