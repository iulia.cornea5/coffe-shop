package coffeshop.products.food;

import coffeshop.products.food.enums.CakeType;
import coffeshop.products.food.enums.FoodSize;

public class Cake extends Food {

    private CakeType cakeType;

    public Cake(int quantity, FoodSize foodSize, CakeType cakeType) {
        super(foodSize);
        float basePrice = -1;
        switch (cakeType) {
            case COOKIE:
                basePrice = 4;
                break;
            case CARROT_CAKE:
                basePrice = 8;
                break;
            case CHEESE_CAKE:
                basePrice = 10;
                break;
        }
        super.setBasePrice(basePrice);
        super.setQuantity(quantity);
        this.cakeType = cakeType;
    }

    public CakeType getCakeType() {
        return cakeType;
    }

    public void setCakeType(CakeType cakeType) {
        this.cakeType = cakeType;
    }

}
