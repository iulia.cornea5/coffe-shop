package coffeshop.products.food.enums;

public enum FoodSize {
    SMALL, LARGE
}
