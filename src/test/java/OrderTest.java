import coffeshop.Order;
import org.junit.jupiter.api.*;
import coffeshop.products.Product;
import coffeshop.products.drink.Drink;
import coffeshop.products.drink.enums.DrinkSize;
import coffeshop.products.drink.enums.DrinkType;
import coffeshop.products.food.Cake;
import coffeshop.products.food.Sandwich;
import coffeshop.products.food.enums.CakeType;
import coffeshop.products.food.enums.FoodSize;
import coffeshop.products.food.enums.SandwichType;

public class OrderTest {

    @BeforeAll
    public static void markTestOrderStarted() {
        System.out.println("START TESTS FOR ORDER");
    }

    @BeforeEach
    public void markTestStarted() {
        System.out.println("-------------------------------------------- Starting test --------------------------------------------");
        System.out.println("");
    }

    @AfterEach
    public void markTestFinished() {
        System.out.println("-------------------------------------------- Finished test --------------------------------------------");
    }


    // THIS IS BAD PRACTICE, Question: what is the bad practice here? How should it have been done?
    @Test
    public void testEverything() {
        System.out.println("-------------------------------------------- TEST TOTAL PRICE --------------------------------------------");

        // given
        System.out.println("Create an order with: " +
                "one Large Late with Alcohol,  " +
                "two Small Hot Chocolates without Alcohol, " +
                "three Small Vegan Sandwiches, " +
                "two Large Cookies ");
        Product oneLargeLateAlcoholic = new Drink(1, DrinkSize.LARGE, DrinkType.LATTE, true);
        Product twoSmallHotChocolateNonAlcoholic = new Drink(2, DrinkSize.SMALL, DrinkType.HOT_CHOCOLATE, false);
        Product threeSmallVeganSandwich = new Sandwich(3, FoodSize.SMALL, SandwichType.VEGAN);
        Product twoLargeCookie = new Cake(2, FoodSize.LARGE, CakeType.COOKIE);
        coffeshop.Order testOrder = new Order();

        // when
        float latePrice = oneLargeLateAlcoholic.getSellingPrice();
        float hotChocolatePrice = twoSmallHotChocolateNonAlcoholic.getSellingPrice();
        float sandwichPrice = threeSmallVeganSandwich.getSellingPrice();
        float cookiePrice = twoLargeCookie.getSellingPrice();
        testOrder.addProduct(oneLargeLateAlcoholic);
        testOrder.addProduct(twoSmallHotChocolateNonAlcoholic);
        testOrder.addProduct(threeSmallVeganSandwich);
        testOrder.addProduct(twoLargeCookie);

        // then
        Assertions.assertEquals(24f, latePrice);
        Assertions.assertEquals(20, hotChocolatePrice);
        Assertions.assertEquals(15, sandwichPrice);
        Assertions.assertEquals(16, cookiePrice);
        Assertions.assertEquals(75, testOrder.getTotalSellingPrice());
        System.out.println("Assert final price of the order is 75");

    }
}
