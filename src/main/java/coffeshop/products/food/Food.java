package coffeshop.products.food;

import coffeshop.products.Product;
import coffeshop.products.food.enums.FoodSize;

public abstract class Food extends Product {
    private FoodSize foodSize;

    public Food(FoodSize foodSize) {
        super();
        this.foodSize = foodSize;
    }

    public FoodSize getFoodSize() {
        return foodSize;
    }

    public void setFoodSize(FoodSize foodSize) {
        this.foodSize = foodSize;
    }

    @Override
    public float getSellingPrice() {
        float sellingPrice = getBasePrice();
        if (FoodSize.LARGE.equals(getFoodSize())) {
            sellingPrice = sellingPrice * 2;
        }
        sellingPrice = sellingPrice * getQuantity();
        return sellingPrice;
    }
}
