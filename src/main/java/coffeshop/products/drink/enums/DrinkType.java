package coffeshop.products.drink.enums;

public enum DrinkType {
    ESPRESSO, LATTE, HOT_CHOCOLATE
}
