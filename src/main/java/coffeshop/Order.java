package coffeshop;

import coffeshop.products.Product;

import java.util.ArrayList;

public class Order {
    private ArrayList<Product> products;

    public Order() {
        products = new ArrayList<Product>();
    }

    public void addProduct(Product p) {
        products.add(p);
    }

    public ArrayList<Product> getProducts() {
        return new ArrayList<>(this.products);
    }

    public float getTotalSellingPrice() {
        float totalSellingPrice = 0;
        for (int i = 0; i < products.size(); i++) {
            float productSellingPrice = products.get(i).getSellingPrice();
            totalSellingPrice = totalSellingPrice + productSellingPrice;
        }
        return totalSellingPrice;
    }
}
