package coffeshop.products;

public abstract class Product {

    private float basePrice;
    private int quantity;

    public Product() {
    }


    public float getBasePrice() {
        return basePrice;
    }

    protected void setBasePrice(float basePrice) {
        this.basePrice = basePrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public abstract float getSellingPrice();
}
