package coffeshop.products.food;

import coffeshop.products.food.enums.FoodSize;
import coffeshop.products.food.enums.SandwichType;

public class Sandwich extends Food {
    private SandwichType sandwichType;

    public Sandwich(int quantity, FoodSize foodSize, SandwichType sandwichType) {
        super(foodSize);
        float basePrice = -1;
        switch (sandwichType) {
            case VEGAN:
                basePrice = 5;
                break;
            case WITH_MEAT:
                basePrice = 10;
                break;
        }
        super.setBasePrice(basePrice);
        super.setQuantity(quantity);
    }

    public SandwichType getSandwichType() {
        return sandwichType;
    }

    public void setSandwichType(SandwichType sandwichType) {
        this.sandwichType = sandwichType;
    }

}
