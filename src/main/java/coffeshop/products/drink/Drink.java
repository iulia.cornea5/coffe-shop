package coffeshop.products.drink;

import coffeshop.products.Product;
import coffeshop.products.drink.enums.DrinkSize;
import coffeshop.products.drink.enums.DrinkType;

public class Drink extends Product {
    private DrinkType drinkType;
    private DrinkSize drinkSize;
    private boolean alcoholic;

    public Drink(int quantity, DrinkSize drinkSize,  DrinkType drinkType, boolean alcoholic) {
        super();
        float basePrice = -1;
        switch (drinkType) {
            case ESPRESSO:
                basePrice = 6;
                break;
            case LATTE:
            case HOT_CHOCOLATE:
                basePrice = 10;
                break;
        }
        super.setBasePrice(basePrice);
        super.setQuantity(quantity);
        this.drinkType = drinkType;
        this.drinkSize = drinkSize;
        this.alcoholic = alcoholic;
    }

    public DrinkType getDrinkType() {
        return drinkType;
    }

    public void setDrinkType(DrinkType drinkType) {
        this.drinkType = drinkType;
    }

    public DrinkSize getDrinkSize() {
        return drinkSize;
    }

    public void setDrinkSize(DrinkSize drinkSize) {
        this.drinkSize = drinkSize;
    }

    public boolean isAlcoholic() {
        return alcoholic;
    }

    public void setAlcoholic(boolean alcoholic) {
        this.alcoholic = alcoholic;
    }

    @Override
    public float getSellingPrice() {
        float sellingPrice = getBasePrice();
        switch (getDrinkSize()) {
            case MEDIUM:
                sellingPrice = sellingPrice * 1.5f;
                break;
            case LARGE:
                sellingPrice = sellingPrice * 2;
                break;
        }
        if(isAlcoholic()) {
            sellingPrice = sellingPrice * 1.2f;
        }
        sellingPrice = sellingPrice * getQuantity();
        return  sellingPrice;
    }
}
